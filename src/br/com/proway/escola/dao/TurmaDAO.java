package br.com.proway.escola.dao;

import br.com.proway.escola.configs.Conexao;
import br.com.proway.escola.models.Turma;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TurmaDAO {
    public ArrayList<Turma> selecionarTurmas() {
        ArrayList<Turma> turmaLista = new ArrayList<>();

        String query = "SELECT * FROM escola.turma";
        try {
            Statement stmt = Conexao.conexao.createStatement();
            ResultSet resultado = stmt.executeQuery(query);
            while (resultado.next()) {
                Turma turma = new Turma();
                turma.setId(resultado.getLong("id"));
                turma.setPeriodo(resultado.getString("periodo"));
                turma.setSemestre(resultado.getInt("semestre"));
                turmaLista.add(turma);
            }
            stmt.close();
            resultado.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }
        return turmaLista;
    }

    public Turma selecionarTurma(long id) {
        Turma turma = new Turma();
        String query = "SELECT * FROM escola.turma WHERE id = " + id;
        try {
            Statement stmt = Conexao.conexao.createStatement();
            ResultSet resultado = stmt.executeQuery(query);
            while (resultado.next()) {
                turma.setId(resultado.getLong("id"));
                turma.setPeriodo(resultado.getString("periodo"));
                turma.setSemestre(resultado.getInt("semestre"));
            }
            stmt.close();
            resultado.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados", JOptionPane.INFORMATION_MESSAGE);
        }
        return turma;
    }

    public Turma inserirTurma(Turma turma) {
        String query = "INSERT INTO escola.turma(periodo,semestre) VALUES(?,?)";

        try {
            PreparedStatement pstmt = Conexao.conexao.prepareStatement(query,
                    Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, turma.getPeriodo());
            pstmt.setInt(2, turma.getSemestre());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try {
                    ResultSet resultado = pstmt.getGeneratedKeys();
                    if (resultado.next()) {
                        turma.setId(resultado.getLong(1));
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                            , JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }
        return turma;
    }

    public void atualizarTurma(Turma turma) {
        String query = "UPDATE escola.turma "
                + "SET periodo = ?, semestre = ? "
                + "WHERE id = ?";
        int affectedrows = 0;

        try {
            PreparedStatement pstmt = Conexao.conexao.prepareStatement(query);
            pstmt.setString(1, turma.getPeriodo());
            pstmt.setInt(2, turma.getSemestre());
            pstmt.setLong(3, turma.getId());
            affectedrows = pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void deletarTurma(long id) {
        String query = "DELETE FROM escola.turma WHERE id = ?";
        int affectedrows = 0;
        try {
            PreparedStatement pstmt = Conexao.conexao.prepareStatement(query);
            pstmt.setLong(1, id);
            affectedrows = pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados", JOptionPane.INFORMATION_MESSAGE);
        }

    }
}
