package br.com.proway.escola.dao;

import br.com.proway.escola.configs.Conexao;
import br.com.proway.escola.models.Pessoa;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PessoaDAO {
    public Pessoa inserirPessoa(Pessoa pessoa) {
        String query = "INSERT INTO escola.pessoa (nome, idade) VALUES (?, ?);";

        try {
            PreparedStatement statement = Conexao.conexao.prepareStatement(query,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, pessoa.getNome());
            statement.setInt(1, pessoa.getIdade());

            int linhasAlteradas = statement.executeUpdate();

            if (linhasAlteradas > 0) {
                try {
                    ResultSet resultado = statement.getGeneratedKeys();

                    if (resultado.next()) {
                        pessoa.setId(resultado.getLong(1));
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                            , JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
        }

        return new Pessoa();
    }
}
