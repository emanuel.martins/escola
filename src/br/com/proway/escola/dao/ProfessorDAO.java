package br.com.proway.escola.dao;

import br.com.proway.escola.configs.Conexao;
import br.com.proway.escola.models.Professor;

import javax.swing.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ProfessorDAO {

    public ArrayList<Professor> selecionarProfessor(){

        ArrayList<Professor> professorArrayList =  new ArrayList<>();
        String query = "SELECT * FROM escola.professor";

        try{
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultado = statement.executeQuery(query);
            while(resultado.next()){
                Professor professor = new Professor();
                professor.setId(resultado.getLong("id"));
                professor.setAtivo(resultado.getBoolean("ativo"));
                professor.setRegistro(resultado.getString("registro"));
                professorArrayList.add(professor);
            }
            statement.close();
            resultado.close();

        }catch (Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados", JOptionPane.INFORMATION_MESSAGE);
        }

        return professorArrayList;
    }

    public Professor selecionarProfessor(long id){
        Professor professor = new Professor();
        String query = "SELECT * FROM escola.professor WHERE id =" + id;
        try{
            Statement stmt = Conexao.conexao.createStatement();
            ResultSet resultado = stmt.executeQuery(query);
            while (resultado.next()) {
                professor.setId(resultado.getLong("id"));
                professor.setAtivo(resultado.getBoolean("ativo"));
                professor.setRegistro(resultado.getString("registro"));
            }

            stmt.close();
            resultado.close();
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        return professor;
    }
}
