package br.com.proway.escola.dao;

import br.com.proway.escola.configs.Conexao;
import br.com.proway.escola.models.Disciplina;


import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DisciplinaDAO {
    public ArrayList<Disciplina> selecionarDisciplinas() {
        ArrayList<Disciplina> disciplinas = new ArrayList<>();
        String query = "SELECT * FROM escola.disciplina";

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Disciplina disciplina = new Disciplina();

                disciplina.setId(resultSet.getLong("id"));
                disciplina.setDescricao(resultSet.getString("descricao"));
                disciplina.setCargaHoraria(resultSet.getString("carga_horaria"));

                disciplinas.add(disciplina);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }

        return disciplinas;
    }

    public Disciplina selecionarDisciplinaPeloID(long id) {
        Disciplina disciplina = new Disciplina();
        String query = "SELECT * FROM escola.disciplina d WHERE d.id = " + id;

        try {
            Statement statement = Conexao.conexao.createStatement();
            ResultSet resultado = statement.executeQuery(query);

            while (resultado.next()) {
                disciplina.setId(resultado.getLong("id"));
                disciplina.setDescricao(resultado.getString("descricao"));
                disciplina.setCargaHoraria(resultado.getString("carga_horaria"));
            }

            statement.close();
            resultado.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Banco de Dados"
                    , JOptionPane.INFORMATION_MESSAGE);
        }

        return disciplina;
    }
}
