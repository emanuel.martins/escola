package br.com.proway.escola;

import br.com.proway.escola.configs.Conexao;
import br.com.proway.escola.dao.DisciplinaDAO;
import br.com.proway.escola.dao.PessoaDAO;
import br.com.proway.escola.dao.ProfessorDAO;
import br.com.proway.escola.dao.TurmaDAO;
import br.com.proway.escola.models.Disciplina;
import br.com.proway.escola.models.Pessoa;
import br.com.proway.escola.models.Professor;
import br.com.proway.escola.models.Turma;

import javax.swing.*;
import java.util.ArrayList;

public class App {
    App() {
        Conexao conexao = new Conexao();

        conexao.conectar();


        /****************************************************************
         *                  Instâncias DAO                              *
         ****************************************************************/
        PessoaDAO pessoaDAO = new PessoaDAO();
        DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
        TurmaDAO turmaDAO = new TurmaDAO();
        ProfessorDAO professorDAO = new ProfessorDAO();

        /****************************************************************
         *                  Testes                                      *
         ****************************************************************/
        ArrayList<Disciplina> listaDisciplinas = disciplinaDAO.selecionarDisciplinas();
        Disciplina disciplinaSelecionada = disciplinaDAO.selecionarDisciplinaPeloID(2);

        ArrayList<Turma> listaTurmas = turmaDAO.selecionarTurmas();
        Turma turmaSelecionada = turmaDAO.selecionarTurma(3L);

        ArrayList<Professor> listaProfessores = professorDAO.selecionarProfessor();
        Professor professorSelecionado = professorDAO.selecionarProfessor(1);

        Turma turma = new Turma();
        Pessoa pessoa = new Pessoa();

        turma.setSemestre(1);
        turma.setPeriodo("Noturno");

        turmaDAO.inserirTurma(turma);
        turma = turmaDAO.selecionarTurma(10);
        turma.setPeriodo("Matutino");

        turmaDAO.atualizarTurma(turma);
        JOptionPane.showMessageDialog(null, turma.getPeriodo());

        turmaDAO.deletarTurma(16);

        pessoa.setNome("Pessoa 01");
        pessoa.setIdade(30);

        pessoaDAO.inserirPessoa(pessoa);

        JOptionPane.showMessageDialog(null, disciplinaSelecionada.getDescricao()
                .concat("\nCarga Horária: " + disciplinaSelecionada.getCargaHoraria()));
        JOptionPane.showMessageDialog(null, mostrarDisciplinas(listaDisciplinas));

        JOptionPane.showMessageDialog(null, mostrarTurmas(listaTurmas));
        JOptionPane.showMessageDialog(null, "" + turmaSelecionada.getSemestre() +
                "\nPeríodo: " + disciplinaSelecionada.getCargaHoraria());

        JOptionPane.showMessageDialog(null, mostrarProfessores(listaProfessores));
        JOptionPane.showMessageDialog(null, professorSelecionado.getId() + ": "
                + "\nRegistro: " + professorSelecionado.getRegistro()
                + "\nAtivo? " + professorSelecionado.getAtivo());

        conexao.fecharConexao();
    }

    public static String mostrarDisciplinas(ArrayList<Disciplina> disciplinas) {
        String texto = "";

        for (int i = 0; i < disciplinas.size(); i++) {
            Disciplina disciplina = disciplinas.get(i);

            texto += disciplina.getId() + ": \n" + "Disciplina: " + disciplina.getDescricao()
                    + ", Carga Horária: " + disciplina.getCargaHoraria() + "\n";
        }

        return texto;
    }

    public static String mostrarTurmas(ArrayList<Turma> turmas) {
        String texto = "";

        for (int i = 0; i < turmas.size(); i++) {
            Turma turma = turmas.get(i);

            texto += turma.getId() + ": \n" + "Semestre: " + turma.getSemestre()
                    + ", Período: " + turma.getPeriodo() + "\n";
        }

        return texto;
    }

    public static String mostrarProfessores(ArrayList<Professor> professores) {
        String texto = "";

        for (int i = 0; i < professores.size(); i++) {
            Professor professor = professores.get(i);

            texto += professor.getId() + ": \n" + "Registro: " + professor.getRegistro()
                    + ", Ativo: " + professor.getAtivo() + "\n";
        }

        return texto;
    }
}
